import json
import requests
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages
from django.core.mail import send_mail
from django.contrib.sites.shortcuts import get_current_site
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.conf import settings
from bets.forms import PlaceBetsForm
from bets.models import ProposedBet, AcceptedBet, UserProfile, UserProfileAudit, User
from django.db.models import Sum

# Create your views here.

def bets(request):
    return HttpResponseRedirect('/bets/my_bets')



@login_required(login_url='/login/')
def my_bets(request):

    # get the current user
    current_user = request.user

    
    #your_accepted_bets = AcceptedBet.objects.select_related('accepted_prop').filter(accepted_user=current_user)

    your_accepted_bets = AcceptedBet.objects.raw("""SELECT "bets_acceptedbet"."id", "bets_acceptedbet"."accepted_prop_id", "bets_acceptedbet"."accepted_user_id", "bets_acceptedbet"."user_od", "bets_acceptedbet"."bet_amount", "bets_proposedbet"."id", "bets_proposedbet"."match", "bets_proposedbet"."ods_home", "bets_proposedbet"."ods_tie", "bets_proposedbet"."ods_away", "bets_proposedbet"."end_date", "bets_proposedbet"."result" FROM "bets_acceptedbet" INNER JOIN "bets_proposedbet" ON ("bets_acceptedbet"."accepted_prop_id" = "bets_proposedbet"."id") WHERE "bets_acceptedbet"."accepted_user_id" = 2 ORDER BY "bets_proposedbet"."end_date" ASC""")


    return render(request,
                  'bets/base_my_bets.html',
                  {'nbar': 'my_bets',
                   'your_accepted_bets': your_accepted_bets})

@login_required(login_url='/login/')
def open_bets(request):

    # used for expiring soon and new bet tags
    tomorrow = timezone.now() + timezone.timedelta(days=1)
    yesterday = timezone.now() + timezone.timedelta(days=-1)

    current_user = request.user

    open_bets = ProposedBet.objects.filter(end_date__gt = timezone.now())


    return render(request,
                  'bets/base_open_bets.html',
                  {'nbar': 'open_bets',
                   'open_bets': open_bets})


class MyCompletedBetsJson(BaseDatatableView):
    order_columns = ['accepted_prop__prop_text',
                     'accepted_prop__prop_wager', '', '']

    def get_initial_queryset(self):
        current_user = self.request.user
        your_accepted_bets = AcceptedBet.objects.filter(
            accepted_user=current_user, accepted_prop__won_bet__isnull=False)

        your_bets_accepted_by_others = AcceptedBet.objects.filter(
            accepted_prop__user=current_user, accepted_prop__won_bet__isnull=False)

        your_closed_bets = your_accepted_bets | your_bets_accepted_by_others
        return your_closed_bets.order_by('-accepted_prop__modified_on')

    def filter_queryset(self, qs):
        sSearch = self.request.GET.get(u'search[value]', None)

        if sSearch:
            qs = qs.filter(
                accepted_prop__prop_text__istartswith=sSearch) | qs.filter(
                accepted_user__first_name__istartswith=sSearch) | qs.filter(
                accepted_user__last_name__istartswith=sSearch) | qs.filter(
                accepted_prop__user__first_name__istartswith=sSearch) | qs.filter(
                    accepted_prop__user__last_name__istartswith=sSearch)

        return qs

    def prepare_results(self, qs):
        json_data = []
        current_user = self.request.user

        for item in qs:

            # figure out who the bet was against
            bet_against_user = ''
            if item.accepted_prop.user == current_user:
                bet_against_user = item.accepted_user.get_full_name()
            else:
                bet_against_user = item.accepted_prop.user.get_full_name()

            who_won = ''
            if item.accepted_prop.get_won_bet_display() == "Win":
                who_won = item.accepted_prop.user.get_full_name()
            elif item.accepted_prop.get_won_bet_display() == "Loss":
                who_won = item.accepted_user.get_full_name()
            else:
                who_won = 'push'

            json_data.append([
                item.accepted_prop.prop_text,
                '$' + str(item.accepted_prop.prop_wager),
                bet_against_user,
                who_won
            ])

        return json_data

class AllBetsJson(BaseDatatableView):
    order_columns = [
        'accepted_prop__user',
        'accepted_user',
        'accepted_prop__prop_text',
        'accepted_prop__prop_wager',
        '']

    def get_initial_queryset(self):
        return AcceptedBet.objects.filter(
            accepted_prop__won_bet__isnull=False).order_by('-accepted_prop__modified_on')

    def filter_queryset(self, qs):
        sSearch = self.request.GET.get(u'search[value]', None)

        if sSearch:
            qs = qs.filter(
                accepted_prop__prop_text__istartswith=sSearch) | qs.filter(
                accepted_user__first_name__istartswith=sSearch) | qs.filter(
                accepted_user__last_name__istartswith=sSearch) | qs.filter(
                accepted_prop__user__first_name__istartswith=sSearch) | qs.filter(
                    accepted_prop__user__last_name__istartswith=sSearch)

        return qs

    def prepare_results(self, qs):
        json_data = []
        for item in qs:

            who_won = ''
            if item.accepted_prop.get_won_bet_display() == "Win":
                who_won = item.accepted_prop.user.get_full_name()
            elif item.accepted_prop.get_won_bet_display() == "Loss":
                who_won = item.accepted_user.get_full_name()
            else:
                who_won = 'push'

            json_data.append([
                item.accepted_prop.user.get_full_name(),
                item.accepted_user.get_full_name(),
                item.accepted_prop.prop_text,
                '$' + str(item.accepted_prop.prop_wager),
                who_won
            ])

        return json_data

class AdminBetsJson(BaseDatatableView):
    order_columns = ['user', 'prop_text', '', '']

    def get_initial_queryset(self):
        closed_accepted_bets = AcceptedBet.objects.filter(
            accepted_prop__won_bet__isnull=False)
        closed_prop_bets = ProposedBet.objects.filter(
            acceptedbet__in=closed_accepted_bets).distinct().order_by('-modified_on')

        return closed_prop_bets

    def filter_queryset(self, qs):
        sSearch = self.request.GET.get(u'search[value]', None)

        if sSearch:
            qs = qs.filter(
                prop_text__istartswith=sSearch) | qs.filter(
                user__first_name__istartswith=sSearch) | qs.filter(
                user__last_name__istartswith=sSearch)

        return qs

    def prepare_results(self, qs):
        json_data = []
        for item in qs:

            completed_bet_info = [
                item.user.get_full_name(),
                item.prop_text,
                item.id]
            json_data.append([
                item.user.get_full_name(),
                item.prop_text,
                item.get_won_bet_display(),
                completed_bet_info
            ])

        return json_data


@csrf_exempt
@login_required(login_url='/login/')
def accept_prop_bet(request):
    print("In accept_prop_bet func")
    if request.method == 'POST':
        # get the prop id from the get request
        bet_id = request.POST.get('id')
        user_od = request.POST.get('user_od')
        bet_amount = request.POST.get('bet_amount')
        print("insede if",bet_id,user_od,bet_amount)
        try:
            int(bet_id)
            is_int = True

        except ValueError:
            is_int = False

        if is_int:
            # make sure bet exists
            try:
                prop_bet = ProposedBet.objects.get(id=bet_id)

            except ProposedBet.DoesNotExist:
                prop_bet = None

            # make sure bet is someone elses and it has bets left and it isn't
            # expired
            if prop_bet :

                accepted_bet = AcceptedBet(
                    accepted_prop=prop_bet,\
                    accepted_user=request.user,\
                    user_od = user_od,\
                    bet_amount = int(bet_amount))
                accepted_bet.save()

                # send a message over that the bet is accepted
                messages.success(request, 'Ставка упешно сделана!')

                # send an email to the propser, if they have their setting
                # enabled
                current_site = get_current_site(request)
                domain = current_site.domain

                
            else:
                # send a message over that there was an error
                messages.error(
                    request, 'Ошибка. Что-то пошло не так..')

        else:
            # send a message over that there was an error
            messages.error(request, 'ID ставки должно быть integer')
    else:
        # send a message over that there was an error
        messages.error(request, 'Что-то пошло не так..')

    return HttpResponseRedirect('/bets/open_bets')

@csrf_exempt
@login_required(login_url='/login/')
def check_duplicate_bet(request):

    if request.method == 'POST':
        # get the id in the post
        prop_id = request.POST.get('id')
        response_data = {}
        # check and see if this user has already accepted this bet at least
        # once
        prop_bet = ProposedBet.objects.get(id=prop_id)
        accepted_bets = AcceptedBet.objects.filter(
            accepted_prop=prop_bet, accepted_user=request.user)
        if accepted_bets:
            response_data['is_duplicate'] = 'True'
        else:
            response_data['is_duplicate'] = 'False'

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this should not happen"}),
            content_type="application/json"
        )
