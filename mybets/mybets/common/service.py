import requests
import pickle
import time
import os
#import own files:
import database as db
import matches_api as api

#saved_model = 'model.sav'
#learning_database = 'match_history.sqlite'
working_database = '/home/artem/Work/coursework/mybets/mybets/db.sqlite3'

def add_future_matches():
	last = 0 #get 10 future matches
	matches_list = api.get_10_matches(last)
	for match in matches_list:
		TEAM_1 = match[0].split('-')[0].strip()
		TEAM_2 = match[0].split('-')[1].strip()
		MATCH_DATE = match[2]
		RESULT = match[1]
		PREDICTION = [1.6,2.5,4.9]
		MATCH_ID = db.chech_match_existence(TEAM_1,TEAM_2, MATCH_DATE,working_database)
		
		if MATCH_ID is None:
			db.add_match(TEAM_1,TEAM_2,MATCH_DATE,PREDICTION,RESULT,working_database)

def update_results():
	last = 1
	matches_list = api.get_10_matches(last)
	for match in matches_list:
		TEAM_1 = match[0].split('-')[0].strip()
		TEAM_2 = match[0].split('-')[1].strip()
		MATCH_DATE = match[2]
		RESULT = match[1]
		MATCH_ID = db.chech_match_existence(TEAM_1,TEAM_2, MATCH_DATE,working_database)
		if MATCH_ID is not None:
			db.update_results_in_db(MATCH_ID, RESULT, working_database)

def delayed(func,delay):
	'''delaying GET queries in (free API usage - 10 calls per minute), delay = n seconds'''
	start = time.time()
	func()
	end = time.time()
	if (end - start)<delay:
		time.sleep(delay - (end - start))


exists = os.path.isfile(working_database)
if exists:
    pass
else:
    db.create_db(working_database)

while True:
	print('Adding new matches...')
	delayed(add_future_matches,60)
	print('Updating matches result...')
	delayed(update_results,60)