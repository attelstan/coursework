import sqlite3

def add_match(TEAM_1,TEAM_2,MATCH_DATE,PREDICTION,RESULT,db_name):
    '''Input vars: TEAM_1,TEAM_2,MATCH_DATE,PREDICTION,RESULT,db_name'''
    conn = sqlite3.connect(db_name)
    c = conn.cursor()    
    query = '''INSERT INTO bets_proposedbet (match,end_date,ods_home,ods_tie,ods_away,result)
    VALUES ("%s", "%s", "%s","%s","%s","%s");''' % (str(TEAM_1)+' - '+str(TEAM_2),str(MATCH_DATE),str(PREDICTION[0]),str(PREDICTION[1]),str(PREDICTION[2]),str(RESULT))
    
    try:
        c.execute(query)
        conn.commit()
        conn.close()
        return ('Match successfully added!')
    except Exception as e:
        conn.close()
        return (e)

def chech_match_existence(TEAM_1,TEAM_2,MATCH_DATE,db_name):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    query = '''SELECT id FROM bets_proposedbet WHERE match = "%s" 
    AND end_date = "%s"''' % (str(TEAM_1)+' - '+str(TEAM_2),str(MATCH_DATE)) 
    try:
        c.execute(query)
        rows = c.fetchall()
    except Exception as e:
        print(e)
        conn.close()
        rows = []
        
    if len(rows)!=0:
        return (rows[0][0])
    else:
        return (None)

def update_results_in_db(ID,result, db_name):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    query = '''UPDATE bets_proposedbet SET result = %s WHERE id = %s''' % (result, ID)
    try:
        c.execute(query)
        conn.commit()
        conn.close()
        return ('User successfully added!')
    except Exception as e:
        conn.close()
        return (e)