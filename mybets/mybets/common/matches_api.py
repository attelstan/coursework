import requests
import datetime

def get_10_matches(last):
	'''last = 0 for 10 future matches, last = 1 for 10 last matches '''
	if last == 0:
		dateFrom = datetime.datetime.now().date()
		dateTo = datetime.datetime.now().date() + datetime.timedelta(days=10)
	else:
		dateFrom = datetime.datetime.now().date() - datetime.timedelta(days=10)
		dateTo = datetime.datetime.now().date() 
	url = 'https://api.football-data.org/v2/matches?competitions=PL&dateFrom=%s&dateTo=%s' %(str(dateFrom),str(dateTo))
	headers = {'X-Auth-Token': '420cdc3d167744d4b7b0b3b570ded89a'}
	r = requests.get(url,headers = headers)
	data = r.json()
	matches = data.get('matches')
	matches_list = []
	if matches != None:
		for match in matches:
			matches_list.append([str(match.get('homeTeam').get('name'))+' - ' + str(match.get('awayTeam').get('name')),
	    	str(match.get('score').get('fullTime').get('homeTeam'))+':'+str(match.get('score').get('fullTime').get('awayTeam')),
	        match.get('utcDate')])
	return (matches_list)

