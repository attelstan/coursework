from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from bets.models import UserProfile


class SignUpForm(UserCreationForm):
    # declare the fields you will show
    username = forms.EmailField(
        label="Логин",
        help_text="Ваша почта будет логином для входа на сайт")
    first_name = forms.CharField(
        label="Имя",
        widget=forms.TextInput(
            attrs={
                'autofocus': 'autofocus'}))
    last_name = forms.CharField(label="Фамилия")

    # this sets the order of the fields

    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "username",
            "password1",
            "password2",
        )

    # this redefines the save function to include the fields you added
    def save(self, commit=True):
        user = super(SignUpForm, self).save(commit=False)
        user.email = self.cleaned_data["username"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]

        if commit:
            user.save()
        return user


class UserProfileForm(forms.Form):
    first_name = forms.CharField(label="Имя", max_length=255)
    last_name = forms.CharField(label="Фамилия", max_length=255)
    email = forms.EmailField(label="Почта", disabled=True, required=False)
